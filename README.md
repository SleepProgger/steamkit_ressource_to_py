Some scripts to convert the protobuf files from SteamKit to python code using `protoc`.  
Also does some dirty translating for enums and such from the LanguageFile folder.  

## HOWTO: ##
 - Download [SteamKit](https://github.com/SteamRE/SteamKit). You only really need the `Resources/Protobufs` and `Resources/SteamLanguage` folder.
 - Move both scripts in this repo to wherever you put the `Ressource/..` folder.
 - Get `protoc.exe` and move it to the same folder.
 - Execute `build_files.bat`.
 - If everything is ok, you should have all protobuf results each in its own folder inside `generated`.  
  The converted `emsg.steamd`, `enums.steamd`, `eresult.steamd` from the LanguageFile folder are located directly inside `generated`.  


*Note to myself: TODO: Make this readme readable and add sh port*