import sys
import re

TAB_SPACE = 4
REPLACE_STRINGS = {
                   'None': 'None_',
                   'True': 'True_',
                   'False': 'False_',                   
                   }

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "USAGE: %s SOURCE DEST" % sys.argv[0]
        exit()
    src, dest = sys.argv[1:]
    print "Reading", src
    with open(src, "rb") as fd_in:
        with open(dest, "wb") as fd_out:
            data = fd_in.read()
            # This is all pretty sloppy. Lets hope we didn't miss something
            # convert enums to classes
            data =  re.sub('enum ([^\r\n \t<]*)[^\r\n]*', r'class \1:', data)
            # Remove ; and comments
            data =  re.sub('([^\r\n]+ *= *[^\r\n;]+) *;[^\r\n]*', r'\1', data)
            # clean up / replace some chars
            data = data.replace(';', '').replace('{', '').replace('}', '')
            data = data.replace('\t', ' '*TAB_SPACE)
            data = data.replace('\r\n', '\n')
            data = data.replace('//', '#')
            # Replace reserved words
            for patt_in, patt_out in REPLACE_STRINGS.items():
                data = data.replace(patt_in, patt_out)        
            fd_out.write(data)