@echo off
REM TODO: port to sh

mkdir generated > nul 2> nul
echo Creating py files from Protobuff...
REM This fails for csgo (TODO: check why), and google (should be ok, as its only used as include)
REM .. plus "dota test" because of the blank, but we don't need that anyway, or do we ?
FOR /D %%G in ("Protobufs/*") DO (
  echo Build %%G
  cd Protobufs\%%G
  rmdir ..\..\generated\%%G  > nul 2> nul
  mkdir ..\..\generated\%%G  > nul 2> nul
  ..\..\protoc.exe --proto_path=.. --proto_path=. --python_out=..\..\generated\%%G *.proto
  cd ..\..
)

echo Creating Language files (EMsg, enums, ...)
python to_py.py SteamLanguage\emsg.steamd generated\emsg.py
python to_py.py SteamLanguage\enums.steamd generated\enums.py
python to_py.py SteamLanguage\eresult.steamd generated\eresult.py

echo Creating __init__.py files ...
FOR /D %%G in (generated\*) DO (
	REM I couldn't get recursively searching for directories to work (damn win)
	REM So lets do it the sloppy way (This only supports one level  of subfolders !)...
	REM TODO: Check if file exists first
	break>%%G/__init__.py
	FOR /D %%F in (%%G\*) DO (
		break>%%F/__init__.py
	)
)
pause